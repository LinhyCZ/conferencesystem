<?php
  class editPostController {
    public function __construct() {
      //Nothing to do
    }

    public function preparePage() {
      $_POST["postData"] = Database::getPostById($_GET["id"]);
    }

    /**
    * Připraví data pro stránku pro nahrání souboru, zpracuje nahrání souboru a uložení příspěvku
    */
    public function showPage() {
      if (isset($_POST["submit"])) {
        $oldPostData = Database::getPostById($_GET["id"]);
        if ($_FILES["file"]["name"] != "") {
          $uploadpath = $_SERVER['DOCUMENT_ROOT'];
          $uploaddir='/media/';
          $uploadfile = $uploadpath.$uploaddir.$_FILES['file']['name'];


          if(!file_exists($uploadfile) || $uploadfile == $oldPostData[0]["File"]) {
            unlink($uploadfile);

            if (move_uploaded_file($_FILES['file']['tmp_name'], $uploadfile)) {
              Database::editPost($_POST["workname"], $_SESSION["ID"], $uploadfile, $_POST["comment"], $_GET["id"]);
              header("Location: /?url=myPosts&edit=success");
            }
          } else {
            header("Location: /?url=editPost&id=" . $_GET["id"] . "&err=fileexists");
          }
        } else {
          Database::editPost($_POST["workname"], $_SESSION["ID"], $oldPostData[0]["File"], $_POST["comment"], $_GET["id"]);
          header("Location: /?url=myPosts&edit=success");
        }
      }
      require("view/patterns/defaultPattern.phtml");
    }
  }
?>
