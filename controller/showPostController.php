<?php
  class showPostController {
    public function __construct() {
      //Nothing to do
    }

    public function preparePage() {
      $_POST["postData"] = Database::getPostById($_GET["id"]);
    }

    /**
    * Připraví data pro stránku, která zobrazí příspěvek
    */
    public function showPage() {
      require("view/patterns/defaultPattern.phtml");
    }
  }
?>
