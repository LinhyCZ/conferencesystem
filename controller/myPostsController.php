<?php
  class myPostsController {
    public function __construct() {
      //Nothing to do
    }

    public function preparePage() {
      $_POST["postsData"] = Database::getUserPosts($_SESSION["ID"]);
    }

    /**
    * Připraví data pro stránku s přípsěvky daného uživatele
    */
    public function showPage() {
      require("view/patterns/defaultPattern.phtml");
    }
  }
?>
