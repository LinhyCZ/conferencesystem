<?php
  class loginController {
    public function __construct() {
      //Nothing to do
    }

    public function preparePage() {
      //Nothing to do
    }

    /**
    * Zpracuje přihlášení, ověří přihlašovací údaje a v případě, že je přihlášení správně, uloží data o uživateli do session
    */
    public function showPage() {
      if (isset($_POST["loginButton"])) {
        if ($_SESSION["loggedIn"] == true) {
          header("Location: /");
        } else {
          $loginData = Database::getLogin($_POST["login"]);

          if (sizeof($loginData) != 1) {
            header("Location: /?url=login&err=username");
          } else {
            if ($loginData[0]["Active"] != 1) {
              header("Location: /?url=login&err=blocked");
            } else {
              if ($loginData[0]["Password"] === $_POST["password"]) {
                $_SESSION["loggedIn"] = true;
                $_SESSION["ID"] = $loginData[0]["ID_Uzivatel"];
                $_SESSION["Nickname"] = $loginData[0]["Nickname"];
                $_SESSION["Name"] = $loginData[0]["Jmeno"];
                $_SESSION["Surname"] = $loginData[0]["Prijmeni"];
                $_SESSION["Email"] = $loginData[0]["Email"];
                $_SESSION["Phone"] = $loginData[0]["Telefon"];
                $_SESSION["Role"] = $loginData[0]["Role"];
                header("Location: /");
              } else {
                header("Location: /?url=login&err=password");
              }
            }
          }
        }
      }

      require("view/patterns/defaultPattern.phtml");
    }
  }
?>
