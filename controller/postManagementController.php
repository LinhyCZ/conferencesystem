<?php
  class postManagementController {
    public function __construct() {
      //Nothing to do
    }

    public function preparePage() {
      $_POST["reviewers"] = Database::getReviewers();
      $_POST["posts"] = Database::getPostsAndReviews();
      $_POST["authors"] = Database::getPostsAuthor();
    }

    /**
    * Připraví data pro správu příspěvků pro admina
    */
    public function showPage() {
      if (isset($_POST["submit"])) {
        Database::addReviewers($_POST["inputPostID"], $_POST["reviewer0"], $_POST["reviewer1"], $_POST["reviewer2"]);
        header("Location: /?url=postManagement&add=success");
      }
      require("view/patterns/defaultPattern.phtml");
    }
  }
?>
