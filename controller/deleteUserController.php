<?php
  class deleteUserController {
    public function __construct() {
      //Nothing to do
    }

    public function preparePage() {
      //Nothing to do
    }


    /**
    * Odstraní uživatele
    */
    public function showPage() {
      if (isset($_GET["id"])) {
        Database::deleteUser($_GET["id"]);
      }

      header("Location: /?url=userManagement");
    }
  }
?>
