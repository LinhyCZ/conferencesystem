<?php
  class postCommentController {
    public function __construct() {
      //Nothing to do
    }

    public function preparePage() {
      //Nothing to do
    }

    /**
    * Vrátí html kód komentáře daného příspěvku
    */
    public function showPage() {
      echo Database::getPostComment($_GET["id"])[0]["Comment"];
    }
  }
?>
