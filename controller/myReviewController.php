<?php
  class myReviewController {
    public function __construct() {
      //Nothing to do
    }

    public function preparePage() {
      $_POST["reviewData"] = Database::getMyReviews($_SESSION["ID"]);
    }

    /**
    * Připraví data pro stránku s recenzemi daného uživatele
    */
    public function showPage() {
      require("view/patterns/defaultPattern.phtml");
    }
  }
?>
