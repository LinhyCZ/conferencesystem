<?php
  class blockController {
    public function __construct() {
      //Nothing to do
    }

    public function preparePage() {
      //Nothing to do
    }


    /**
    * Zablokuje uživatele
    */
    public function showPage() {
      if (isset($_GET["id"])) {
        Database::blockUser($_GET["id"]);
      }

      header("Location: /?url=userManagement");
    }
  }
?>
