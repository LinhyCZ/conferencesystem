<?php
  class registerController {
    public function __construct() {
      //Nothing to do
    }

    public function preparePage() {
      //Nothing to do
    }

    /*
    * Oveří základní správnost vložených dat a zaregistruje uživatele
    */
    public function showPage() {
      if(isset($_POST["submit"])) {
        if ($_POST["username"] == "") {
          header("Location: /?url=register&err=noUsername");
        } else if ($_POST["password"] == "") {
          header("Location: /?url=register&err=noPassword");
        } else if ($_POST["email"] == "") {
          header("Location: /?url=register&err=noEmail");
        } else if ($_POST["name"] == "") {
          header("Location: /?url=register&err=noName");
        } else if ($_POST["surname"] == "") {
          header("Location: /?url=register&err=noSurname");
        } else if (sizeof(Database::getLogin($_POST["username"])) != 0) {
          header("Location: /?url=register&err=usernameExists");
        } else if (sizeof(Database::getEmail($_POST["email"])) != 0) {
          header("Location: /?url=register&err=emailExists");
        } else {
          Database::register($_POST["username"], $_POST["password"], $_POST["email"], $_POST["name"], $_POST["surname"], $_POST["phone"]);
          header("Location: /?url=register&err=success");
        }
      }

      require("view/patterns/defaultPattern.phtml");
    }
  }
?>
