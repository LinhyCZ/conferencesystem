<?php
  class newPostController {
    public function __construct() {
      //Nothing to do
    }

    public function preparePage() {
      //Nothing to do
    }

    /**
    * Zpracuje vytvoření nového příspěvku, uloží nahraný soubor na server
    */
    public function showPage() {
      if (isset($_SESSION["loggedIn"]) && $_SESSION["loggedIn"] == true && $_SESSION["Role"] == 3) {
        if (isset($_POST["submit"])) {
          $uploadpath = $_SERVER['DOCUMENT_ROOT'];
          $uploaddir='/media/';

          $uploadfile = $uploadpath.$uploaddir.$_FILES['file']['name'];

          if (file_exists($uploadfile)) {
            header("Location: /?url=newPost&err=fileexists"); //TODO ERROR
          } else {
            if (move_uploaded_file($_FILES['file']['tmp_name'], $uploadfile)) {
              Database::uploadFile($_POST["workname"], $_SESSION["ID"], $uploadfile, $_POST["comment"]);
            }
          }

          header("Location: /?url=myPosts&add=success");
        }
        require("view/patterns/defaultPattern.phtml");
      } else {
        header("Location: /");
      }
    }
  }
?>
