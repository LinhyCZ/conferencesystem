<?php
  class unblockController {
    public function __construct() {
      //Nothing to do
    }

    public function preparePage() {
      //Nothing to do
    }

    /**
    * Odblokuje uživatele
    */
    public function showPage() {
      if (isset($_GET["id"])) {
        Database::unblockUser($_GET["id"]);
      }

      header("Location: /?url=userManagement");
    }
  }
?>
