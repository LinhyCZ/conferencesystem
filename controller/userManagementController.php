<?php
  class userManagementController {
    public function __construct() {
      //Nothing to do
    }

    public function preparePage() {
      $_POST["usersData"] = Database::getAllUsers();
    }

    /**
    * Připraví data pro stránku se správou uživatelů
    */
    public function showPage() {
      require("view/patterns/defaultPattern.phtml");
    }
  }
?>
