<?php
  class removePostController {
    public function __construct() {
      //Nothing to do
    }

    public function preparePage() {
      //Nothing to do
    }

    /**
    * Odstraní příspěvek
    */
    public function showPage() {
      $filepath = Database::removePost($_GET["id"]);
      unlink($filepath[0]["File"]);

      header("Location: /?url=myPosts");
    }
  }
?>
