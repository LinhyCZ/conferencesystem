<?php
  class reviewCommentController {
    public function __construct() {
      //Nothing to do
    }

    public function preparePage() {
      //Nothing to do
    }

    /**
    * Vrátí html kód komentáře dané recenze
    */
    public function showPage() {
      echo Database::getReviewComment($_GET["id"])[0]["Recomendation"];
    }
  }
?>
