<?php
  class editUserController {
    public function __construct() {
      //Nothing to do
    }

    public function preparePage() {
      $_POST["userData"] = Database::getLoginById($_GET["id"]);
    }

    /*
    * Oveří základní správnost vložených dat a upraví uživatele
    */
    public function showPage() {
      if (isset($_POST["submit"])) {
        $data = Database::getLoginById($_GET["id"]);
        if ($_POST["username"] == "") {
          header("Location: /?url=editUser&id=" . $_GET["id"] . "&err=noUsername");
        } else if ($_POST["password"] == "") {
          header("Location: /?url=editUser&id=" . $_GET["id"] . "&err=noPassword");
        } else if ($_POST["email"] == "") {
          header("Location: /?url=editUser&id=" . $_GET["id"] . "&err=noEmail");
        } else if ($_POST["name"] == "") {
          header("Location: /?url=editUser&id=" . $_GET["id"] . "&err=noName");
        } else if ($_POST["surname"] == "") {
          header("Location: /?url=editUser&id=" . $_GET["id"] . "&err=noSurname");
        } else if (sizeof(Database::getLogin($_POST["username"])) != 0 && $_POST["username"] != $data[0]["Nickname"]) {
          header("Location: /?url=editUser&id=" . $_GET["id"] . "&err=usernameExists");
        } else if (sizeof(Database::getEmail($_POST["email"])) != 0 && $_POST["email"] != $data[0]["Email"]) {
          header("Location: /?url=editUser&id=" . $_GET["id"] . "&err=emailExists");
        } else {
          Database::updateUser($_GET["id"], $_POST["username"], $_POST["password"], $_POST["email"], $_POST["name"], $_POST["surname"], $_POST["phone"], $_POST["role"]);
          header("Location: /?url=userManagement");
        }
      } else {
        require("view/patterns/defaultPattern.phtml");
      }
    }
  }
?>
