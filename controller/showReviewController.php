<?php
  class showReviewController {
    public function __construct() {
      //Nothing to do
    }

    public function preparePage() {
      $_POST["reviewData"] = Database::getReviewByPostId($_GET["id"]);
    }

    public function showPage() {
      require("view/patterns/defaultPattern.phtml");
    }
  }
?>
