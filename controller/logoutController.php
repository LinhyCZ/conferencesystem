<?php
  class logoutController {
    public function __construct() {
      //Nothing to do
    }

    public function preparePage() {
      //Nothing to do
    }

    /**
    * Odhlásí uživatele (Uvolní session)
    */
    public function showPage() {
      session_destroy();
      header("Location: /");
    }
  }
?>
