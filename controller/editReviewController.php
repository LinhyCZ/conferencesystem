<?php
  class editReviewController {
    public function __construct() {
      //Nothing to do
    }

    public function preparePage() {
      $_POST["reviewData"] = Database::getReviewById($_GET["id"]);
    }

    /**
    * Zobrazí stránku pro upravení hodnocení, zpracuje uložení hodnocení
    */
    public function showPage() {
      if (isset($_POST["submit"])) {
        Database::submitReview($_GET["id"], $_POST["originality"], $_POST["quality"], $_POST["length"], $_POST["language"], $_POST["overall"], $_POST["comment"], $_POST["postID"]);

        header("Location: /?url=myReview&edit=success");
      }

      require("view/patterns/defaultPattern.phtml");
    }
  }
?>
