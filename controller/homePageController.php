<?php
  class homePageController {
    public function __construct() {
      //Nothing to do
    }

    public function preparePage() {
      //Nothing to do
    }

    /*
    * Zobrazí domovskou stránku
    */
    public function showPage() {
      require("view/patterns/defaultPattern.phtml");
    }
  }
?>
