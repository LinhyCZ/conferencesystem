<?php
  class blockPostController {
    public function __construct() {
      //Nothing to do
    }

    public function preparePage() {
      //Nothing to do
    }


    /**
    * Nastaví post jako neschávlený
    */
    public function showPage() {
      Database::blockPost($_GET["id"]);

      header("Location: /?url=postManagement&block=success");
    }
  }
?>
