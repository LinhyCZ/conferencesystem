<?php
  class publicPostsController {
    public function __construct() {
      //Nothing to do
    }

    public function preparePage() {
      $_POST["postsData"] = Database::getPublicPosts();
    }

    /**
    * Připraví data pro stránku s veřejnými příspěvky
    */
    public function showPage() {
      require("view/patterns/defaultPattern.phtml");
    }
  }
?>
