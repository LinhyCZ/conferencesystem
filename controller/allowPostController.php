<?php
  class allowPostController {
    public function __construct() {
      //Nothing to do
    }

    public function preparePage() {
      //Nothing to do
    }

    /**
    * Nastaví post jako schávlený
    */
    public function showPage() {
      Database::allowPost($_GET["id"]);

      header("Location: /?url=postManagement&allow=success");
    }
  }
?>
