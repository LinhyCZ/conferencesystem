<?php
  class Database {
    private static $connection;

    /**
    * Naváže spojení s databází
    */
    function __construct() {
      require "DatabaseConfig.php";

      try {
        $pdoString = "mysql:host=" .$serverIP
            . ";dbname=" . $databaseName
            . ";charset=utf8";

        self::$connection = new pdo($pdoString, $username, $password);
  	    self::$connection-> setattribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
      } catch(pdoexception $error) {
        echo "Nepodařilo se připojit k databázi! Error: " . $error->getmessage();
      }
    }

    /**
    * Dotazy zajišťující zjištění informací o uživateli
    */
    static function getLogin($username) {
      if (self::$connection == NULL) {
        die("Nelze se připojit k databázi!");
      }

      $queryString = "SELECT * FROM users WHERE Nickname = :username";

      $query = self::$connection->prepare($queryString);
      $query->bindparam(":username", $username);
      $query->execute();

      return $query->fetchall();
    }

    static function getLoginById($id) {
      if (self::$connection == NULL) {
        die("Nelze se připojit k databázi!");
      }

      $queryString = "SELECT * FROM users WHERE ID_Uzivatel = :id";

      $query = self::$connection->prepare($queryString);
      $query->bindparam(":id", $id);
      $query->execute();

      return $query->fetchall();
    }

    static function getEmail($email) {
      if (self::$connection == NULL) {
        die("Nelze se připojit k databázi!");
      }

      $queryString = "SELECT * FROM users WHERE Email = :email";

      $query = self::$connection->prepare($queryString);
      $query->bindparam(":email", $email);
      $query->execute();

      return $query->fetchall();
    }

    static function register($username, $password, $email, $name, $surname, $phone) {
      if (self::$connection == NULL) {
        die("Nelze se připojit k databázi!");
      }

      $queryString = "INSERT INTO users (Nickname, Password, Jmeno, Prijmeni, Email, Telefon) VALUES (:username, :password, :name, :surname, :email, :phone)";

      $query = self::$connection->prepare($queryString);
      $query->bindparam(":username", $username);
      $query->bindparam(":password", $password);
      $query->bindparam(":name", $name);
      $query->bindparam(":surname", $surname);
      $query->bindparam(":email", $email);
      $query->bindparam(":phone", $phone);

      $query->execute();
    }

    static function updateUser($id, $username, $password, $email, $name, $surname, $phone, $role) {
      if (self::$connection == NULL) {
        die("Nelze se připojit k databázi!");
      }

      $queryString = "UPDATE users SET Nickname = :username, Password = :password, Jmeno = :name, Prijmeni = :surname, Email = :email, Telefon = :phone, Role = :role WHERE ID_Uzivatel = :id";

      $query = self::$connection->prepare($queryString);
      $query->bindparam(":id", $id);
      $query->bindparam(":username", $username);
      $query->bindparam(":password", $password);
      $query->bindparam(":name", $name);
      $query->bindparam(":surname", $surname);
      $query->bindparam(":email", $email);
      $query->bindparam(":phone", $phone);
      $query->bindparam(":role", $role);

      $query->execute();
    }

    static function getAllUsers() {
      if (self::$connection == NULL) {
        die("Nelze se připojit k databázi!");
      }

      $queryString = "SELECT * FROM users";
      $query = self::$connection->prepare($queryString);
      $query->execute();

      return $query->fetchall();
    }

    static function blockUser($id) {
      if (self::$connection == NULL) {
        die("Nelze se připojit k databázi!");
      }

      $queryString = "UPDATE users SET Active = 0 WHERE ID_Uzivatel = :id";
      $query = self::$connection->prepare($queryString);
      $query->bindparam(":id", $id);
      $query->execute();
    }

    static function unblockUser($id) {
      if (self::$connection == NULL) {
        die("Nelze se připojit k databázi!");
      }

      $queryString = "UPDATE users SET Active = 1 WHERE ID_Uzivatel = :id";
      $query = self::$connection->prepare($queryString);
      $query->bindparam(":id", $id);
      $query->execute();
    }

    static function deleteUser($id) {
      if (self::$connection == NULL) {
        die("Nelze se připojit k databázi!");
      }

      $queryString = "UPDATE users SET Active = 2 WHERE ID_Uzivatel = :id";
      $query = self::$connection->prepare($queryString);
      $query->bindparam(":id", $id);
      $query->execute();
    }

    /**
    * Dotazy zajišťující zpracování příspěvků
    */
    static function uploadFile($name, $author, $filename, $comment) {
      if (self::$connection == NULL) {
        die("Nelze se připojit k databázi!");
      }

      $queryString = "INSERT INTO posts (Nazev, Autor, File, Comment) VALUES (:nazev, :autor, :file, :comment)";
      $query = self::$connection->prepare($queryString);
      $query->bindparam(":nazev", $name);
      $query->bindparam(":autor", $author);
      $query->bindparam(":file", $filename);
      $query->bindparam(":comment", $comment);

      $query->execute();
    }

    static function getUserPosts($id) {
      if (self::$connection == NULL) {
        die("Nelze se připojit k databázi!");
      }

      $queryString = "SELECT * FROM posts WHERE Autor = :id";

      $query = self::$connection->prepare($queryString);
      $query->bindparam(":id", $id);
      $query->execute();

      return $query->fetchall();
    }

    static function getPostById($id) {
      if (self::$connection == NULL) {
        die("Nelze se připojit k databázi!");
      }

      $queryString = "SELECT * FROM posts WHERE ID_Post = :id";

      $query = self::$connection->prepare($queryString);
      $query->bindparam(":id", $id);
      $query->execute();

      return $query->fetchall();
    }

    static function removePost($id) {
      if (self::$connection == NULL) {
        die("Nelze se připojit k databázi!");
      }

      $queryString = "SELECT File FROM posts WHERE ID_Post = :id";

      $query = self::$connection->prepare($queryString);
      $query->bindparam(":id", $id);
      $query->execute();

      $toReturn = $query->fetchall();

      $queryString2 = "DELETE FROM reviews WHERE ID_Post = :id";

      $query2 = self::$connection->prepare($queryString2);
      $query2->bindparam(":id", $id);
      $query2->execute();

      $queryString3 = "DELETE FROM posts WHERE ID_Post = :id";

      $query3 = self::$connection->prepare($queryString3);
      $query3->bindparam(":id", $id);
      $query3->execute();

      return $toReturn;
    }

    static function editPost($workname, $authorID, $filename, $comment, $postID) {
      if (self::$connection == NULL) {
        die("Nelze se připojit k databázi!");
      }

      $queryString = "UPDATE posts SET Nazev = :workname, Autor = :authorID, File = :filename, Comment = :comment WHERE ID_Post = :postID";

      $query = self::$connection->prepare($queryString);
      $query->bindparam(":workname", $workname);
      $query->bindparam(":authorID", $authorID);
      $query->bindparam(":filename", $filename);
      $query->bindparam(":comment", $comment);
      $query->bindparam(":postID", $postID);

      $query->execute();
    }

    static function getPostComment($id) {
      if (self::$connection == NULL) {
        die("Nelze se připojit k databázi!");
      }

      $queryString = "SELECT Comment FROM posts WHERE ID_Post = :id";
      $query = self::$connection->prepare($queryString);
      $query->bindparam(":id", $id);
      $query->execute();

      return $query->fetchall();
    }

    static function getPostsAuthor() {
      if (self::$connection == NULL) {
        die("Nelze se připojit k databázi!");
      }

      $queryString = "SELECT Jmeno, Prijmeni, ID_Post FROM users INNER JOIN posts ON posts.Autor = users.ID_Uzivatel";
      $query = self::$connection->prepare($queryString);
      $query->execute();

      return $query->fetchall();
    }

    static function blockPost($id) {
      if (self::$connection == NULL) {
        die("Nelze se připojit k databázi!");
      }

      $queryString = "UPDATE posts SET Status = 2 WHERE ID_Post = :id";
      $query = self::$connection->prepare($queryString);
      $query->bindparam(":id", $id);
      $query->execute();
    }

    static function allowPost($id) {
      if (self::$connection == NULL) {
        die("Nelze se připojit k databázi!");
      }

      $queryString = "UPDATE posts SET Status = 3 WHERE ID_Post = :id";
      $query = self::$connection->prepare($queryString);
      $query->bindparam(":id", $id);
      $query->execute();
    }

    static function getPublicPosts() {
      if (self::$connection == NULL) {
        die("Nelze se připojit k databázi!");
      }

      $queryString = "SELECT * FROM users INNER JOIN posts ON posts.Autor = users.ID_Uzivatel WHERE Status = 3";
      $query = self::$connection->prepare($queryString);
      $query->execute();

      return $query->fetchall();
    }

    /**
    * Dotazy zajišťující zpracování recenzí
    */
    static function getMyReviews($id) {
      if (self::$connection == NULL) {
        die("Nelze se připojit k databázi!");
      }

      $queryString = "SELECT * FROM reviews INNER JOIN posts ON reviews.ID_Post = posts.ID_Post WHERE ID_Reviewer = :id";
      $query = self::$connection->prepare($queryString);
      $query->bindparam(":id", $id);
      $query->execute();

      return $query->fetchall();
    }

    static function getReviewById($id) {
      if (self::$connection == NULL) {
        die("Nelze se připojit k databázi!");
      }

      $queryString = "SELECT * FROM reviews INNER JOIN posts ON reviews.ID_Post = posts.ID_Post WHERE ID_review = :id";
      $query = self::$connection->prepare($queryString);
      $query->bindparam(":id", $id);
      $query->execute();

      return $query->fetchall();
    }

    static function getReviewByPostId($id) {
      if (self::$connection == NULL) {
        die("Nelze se připojit k databázi!");
      }

      $queryString = "SELECT * FROM reviews INNER JOIN posts ON reviews.ID_Post = posts.ID_Post WHERE reviews.ID_Post = :id";
      $query = self::$connection->prepare($queryString);
      $query->bindparam(":id", $id);
      $query->execute();

      return $query->fetchall();
    }

    static function getReviewComment($id) {
      if (self::$connection == NULL) {
        die("Nelze se připojit k databázi!");
      }

      $queryString = "SELECT Recomendation FROM reviews WHERE ID_Review = :id";
      $query = self::$connection->prepare($queryString);
      $query->bindparam(":id", $id);
      $query->execute();

      return $query->fetchall();
    }

    static function submitReview($reviewID, $originality, $quality, $length, $language, $overall, $comment, $postID) {
      if (self::$connection == NULL) {
        die("Nelze se připojit k databázi!");
      }

      $queryString = "UPDATE reviews SET Originality = :originality, Quality = :quality, Length = :length, Language = :language, Overall = :overall, Recomendation = :comment, ReviewStatus = 1 WHERE ID_review = :reviewID";

      $query = self::$connection->prepare($queryString);
      $query->bindparam(":originality", $originality);
      $query->bindparam(":quality", $quality);
      $query->bindparam(":length", $length);
      $query->bindparam(":language", $language);
      $query->bindparam(":overall", $overall);
      $query->bindparam(":comment", $comment);
      $query->bindparam(":reviewID", $reviewID);

      $query->execute();

      $queryString2 = "UPDATE posts SET Status = 1 WHERE ID_Post = :postID";
      $query2 = self::$connection->prepare($queryString2);
      $query2->bindparam(":postID", $postID);
      $query2->execute();
    }

    static function getReviewers() {
      if (self::$connection == NULL) {
        die("Nelze se připojit k databázi!");
      }

      $queryString = "SELECT * FROM users WHERE Role = 2 AND Active = 1";
      $query = self::$connection->prepare($queryString);
      $query->execute();

      return $query->fetchall();
    }

    static function getPostsAndReviews() {
      if (self::$connection == NULL) {
        die("Nelze se připojit k databázi!");
      }

      $queryString = "SELECT * FROM reviews RIGHT JOIN posts ON reviews.ID_Post = posts.ID_Post LEFT JOIN users ON users.ID_Uzivatel = reviews.ID_Reviewer ORDER BY posts.ID_Post ASC";
      $query = self::$connection->prepare($queryString);
      $query->execute();

      return $query->fetchall();
    }

    static function addReviewers($idPost, $reviewer0, $reviewer1, $reviewer2) {
      if (self::$connection == NULL) {
        die("Nelze se připojit k databázi!");
      }

      $queryString = "INSERT INTO reviews (ID_Post, ID_Reviewer) VALUES (:id, :reviewer0), (:id, :reviewer1), (:id, :reviewer2)";
      $query = self::$connection->prepare($queryString);
      $query->bindparam(":id", $idPost);
      $query->bindparam(":reviewer0", $reviewer0);
      $query->bindparam(":reviewer1", $reviewer1);
      $query->bindparam(":reviewer2", $reviewer2);
      $query->execute();
    }
  }
?>
