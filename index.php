<?php

session_start();

function loadController($controllerClass){
  if (mb_strpos($controllerClass, "Controller") == false) {
    require("model/$controllerClass.php");
  } else {
    require("controller/$controllerClass.php");
  }
}

spl_autoload_register("loadController");

if (isset($_GET['url'])) {
  $controllerClass = $_GET['url']."Controller";
  $controller = new $controllerClass;
} else {
  $controller = new homePageController();
}

$database = new Database();
$controller->preparePage();
$controller->showPage();

?>
