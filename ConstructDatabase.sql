-- phpMyAdmin SQL Dump
-- version 4.9.1
-- https://www.phpmyadmin.net/
--
-- Počítač: 127.0.0.1
-- Vytvořeno: Stř 08. led 2020, 23:05
-- Verze serveru: 10.4.8-MariaDB
-- Verze PHP: 7.3.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Databáze: `konference`
--

CREATE DATABASE konference;
USE konference;


-- --------------------------------------------------------

--
-- Struktura tabulky `posts`
--

CREATE TABLE `posts` (
  `ID_Post` int(11) NOT NULL,
  `Nazev` varchar(255) NOT NULL,
  `Autor` int(11) NOT NULL,
  `File` varchar(255) NOT NULL,
  `Status` int(11) NOT NULL DEFAULT 0,
  `Comment` text NOT NULL,
  `Added` datetime NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Vypisuji data pro tabulku `posts`
--

INSERT INTO `posts` (`ID_Post`, `Nazev`, `Autor`, `File`, `Status`, `Comment`, `Added`) VALUES
(6, 'Stromy', 3, 'C:/xampp/htdocs/media/Stromy.pdf', 0, '<p><strong>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. </strong>Pellentesque ipsum. Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Praesent id justo in neque elementum ultrices. Nullam dapibus fermentum ipsum. Nullam faucibus mi quis velit. Praesent in mauris eu tortor porttitor accumsan. Vivamus porttitor turpis ac leo. Nullam eget nisl. Aliquam ante. Nullam justo enim, consectetuer nec, ullamcorper ac, vestibulum in, elit. Duis pulvinar. Praesent id justo in neque elementum ultrices. Sed vel lectus. Donec odio tempus molestie, porttitor ut, iaculis quis, sem. Pellentesque ipsum. Etiam ligula pede, sagittis quis, interdum ultricies, scelerisque eu. Sed elit dui, pellentesque a, faucibus vel, interdum nec, diam. Nullam justo enim, consectetuer nec, ullamcorper ac, vestibulum in, elit. Integer rutrum, orci vestibulum ullamcorper ultricies, lacus quam ultricies odio, vitae placerat pede sem sit amet enim. <i>Pellentesque sapien.</i></p><p>Duis condimentum augue id magna semper rutrum. Pellentesque arcu. Praesent in mauris eu tortor porttitor accumsan. Nulla non arcu lacinia neque faucibus fringilla. Fusce dui leo, imperdiet in, aliquam sit amet, feugiat eu, orci. Aenean placerat. Nunc dapibus tortor vel mi dapibus sollicitudin. Aliquam ornare wisi eu metus. Vestibulum fermentum tortor id mi. Sed convallis magna eu sem. Aliquam ante. Maecenas ipsum velit, consectetuer eu lobortis ut, dictum at dui. Vestibulum fermentum tortor id mi. Sed convallis magna eu sem. Quisque porta. Maecenas libero. Nullam rhoncus aliquam metus.</p><p>Integer lacinia. Etiam dui sem, fermentum vitae, sagittis id, malesuada in, quam. Sed vel lectus. Donec odio tempus molestie, porttitor ut, iaculis quis, sem. Etiam ligula pede, sagittis quis, interdum ultricies, scelerisque eu. Fusce tellus odio, dapibus id fermentum quis, suscipit id erat. Praesent id justo in neque elementum ultrices. In convallis. Suspendisse sagittis ultrices augue. Maecenas aliquet accumsan leo. Nulla turpis magna, cursus sit amet, suscipit a, interdum id, felis. Praesent vitae arcu tempor neque lacinia pretium. Maecenas aliquet accumsan leo. Sed convallis magna eu sem. Pellentesque ipsum. Curabitur vitae diam non enim vestibulum interdum. Nulla accumsan, elit sit amet varius semper, nulla mauris mollis quam, tempor suscipit diam nulla vel leo. Duis ante orci, molestie vitae vehicula venenatis, tincidunt ac pede. Pellentesque arcu. Mauris dictum facilisis augue.</p>', '2020-01-08 22:36:47'),
(7, 'Komprese', 6, 'C:/xampp/htdocs/media/Komprese.pdf', 0, '<p><strong>Lorem ipsum dolor sit amet, consectetuer adipiscing elit.</strong> Aliquam erat volutpat. Morbi scelerisque luctus velit. Integer rutrum, orci vestibulum ullamcorper ultricies, lacus quam ultricies odio, vitae placerat pede sem sit amet enim. Nulla quis diam. Aliquam ornare wisi eu metus. Nam libero tempore, cum soluta nobis est eligendi optio cumque nihil impedit quo minus id quod maxime placeat facere possimus, omnis voluptas assumenda est, omnis dolor repellendus. Pellentesque sapien. Curabitur ligula sapien, pulvinar a vestibulum quis, facilisis vel sapien. Etiam commodo dui eget wisi. Ut tempus purus at lorem. Vivamus ac leo pretium faucibus. Duis pulvinar. In laoreet, magna id viverra tincidunt, sem odio bibendum justo, vel imperdiet sapien wisi sed libero. Nullam at arcu a est sollicitudin euismod. Etiam bibendum elit eget erat. Etiam ligula pede, sagittis quis, interdum ultricies, scelerisque eu.</p><p><strong>Proin pede metus, vulputate nec, fermentum fringilla, vehicula vitae, justo. In enim a arcu imperdiet malesuada. Mauris dolor felis, sagittis at, luctus sed, aliquam non, tellus. Fusce nibh. Duis ante orci, molestie vitae vehicula venenatis, tincidunt ac pede. Etiam dui sem, fermentum vitae, sagittis id, malesuada in, quam. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Aliquam erat volutpat. Sed vel lectus. Donec odio tempus molestie, porttitor ut, iaculis quis, sem. Sed convallis magna eu sem. Nulla non arcu lacinia neque faucibus fringilla. Curabitur sagittis hendrerit ante. Pellentesque ipsum. Nam sed tellus id magna elementum tincidunt. Phasellus enim erat, vestibulum vel, aliquam a, posuere eu, velit. Pellentesque arcu.</strong></p><p>Integer lacinia. Mauris elementum mauris vitae tortor. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Fusce wisi. Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur? Morbi leo mi, nonummy eget tristique non, rhoncus non leo. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos hymenaeos. Phasellus et lorem id felis nonummy placerat. Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean id metus id velit ullamcorper pulvinar. Morbi leo mi, nonummy eget tristique non, rhoncus non leo. Nullam eget nisl. Aenean fermentum risus id tortor.</p>', '2020-01-08 22:37:57'),
(8, 'Semestrální práce Databázové systémy 01', 8, 'C:/xampp/htdocs/media/Dokumentace DB1 - Tomáš Linhart.pdf', 3, '<p><i>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Curabitur ligula sapien, pulvinar a vestibulum quis, facilisis vel sapien. Aliquam in lorem sit amet leo accumsan lacinia. Nam sed tellus id magna elementum tincidunt. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Fusce aliquam vestibulum ipsum. Cras pede libero, dapibus nec, pretium sit amet, tempor quis. Nunc dapibus tortor vel mi dapibus sollicitudin. Etiam sapien elit, consequat eget, tristique non, venenatis quis, ante. Integer tempor. Nullam dapibus fermentum ipsum.</i></p><p><i>Nullam rhoncus aliquam metus. Pellentesque ipsum. Nulla accumsan, elit sit amet varius semper, nulla mauris mollis quam, tempor suscipit diam nulla vel leo. Etiam sapien elit, consequat eget, tristique non, venenatis quis, ante. Duis bibendum, lectus ut viverra rhoncus, dolor nunc faucibus libero, eget facilisis enim ipsum id lacus. Aliquam id dolor. Integer pellentesque quam vel velit. Nullam eget nisl. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Vivamus porttitor turpis ac leo. Maecenas lorem. Nulla est. Mauris tincidunt sem sed arcu. Aenean id metus id velit ullamcorper pulvinar. Integer tempor.</i></p><p><i>Quisque tincidunt scelerisque libero. Etiam quis quam. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Morbi leo mi, nonummy eget tristique non, rhoncus non leo. Nullam lectus justo, vulputate eget mollis sed, tempor sed magna. Nulla est. Vivamus porttitor turpis ac leo. Praesent in mauris eu tortor porttitor accumsan. Sed elit dui, pellentesque a, faucibus vel, interdum nec, diam. Nulla pulvinar eleifend sem. Maecenas ipsum velit, consectetuer eu lobortis ut, dictum at dui.</i></p>', '2020-01-08 22:39:23');

-- --------------------------------------------------------

--
-- Struktura tabulky `reviews`
--

CREATE TABLE `reviews` (
  `ID_review` int(11) NOT NULL,
  `ID_Post` int(11) NOT NULL,
  `ID_Reviewer` int(11) NOT NULL,
  `Originality` int(11) NOT NULL DEFAULT 0,
  `Length` int(11) NOT NULL DEFAULT 0,
  `Quality` int(11) NOT NULL DEFAULT 0,
  `Overall` int(11) NOT NULL DEFAULT 0,
  `Language` int(11) NOT NULL DEFAULT 0,
  `Recomendation` text NOT NULL,
  `ReviewTimestamp` datetime NOT NULL DEFAULT current_timestamp(),
  `ReviewStatus` int(11) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Vypisuji data pro tabulku `reviews`
--

INSERT INTO `reviews` (`ID_review`, `ID_Post`, `ID_Reviewer`, `Originality`, `Length`, `Quality`, `Overall`, `Language`, `Recomendation`, `ReviewTimestamp`, `ReviewStatus`) VALUES
(26, 6, 12, 0, 0, 0, 0, 0, '', '2020-01-08 22:43:48', 0),
(27, 6, 11, 0, 0, 0, 0, 0, '', '2020-01-08 22:43:48', 0),
(28, 6, 13, 0, 0, 0, 0, 0, '', '2020-01-08 22:43:48', 0),
(29, 8, 9, 1, 8, 2, 3, 4, '<h3><i><strong>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. </strong></i></h3><p><i><strong>Phasellus enim erat, vestibulum vel, aliquam a, posuere eu, velit. Nullam justo enim, consectetuer nec, ullamcorper ac, vestibulum in, elit. In convallis. Vivamus ac leo pretium faucibus. Donec vitae arcu. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos hymenaeos. Suspendisse sagittis ultrices augue. Maecenas sollicitudin. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Fusce suscipit libero eget elit.</strong></i></p><p><i><strong>Integer rutrum, orci vestibulum ullamcorper ultricies, lacus quam ultricies odio, vitae placerat pede sem sit amet enim. Aliquam in lorem sit amet leo accumsan lacinia. Suspendisse sagittis ultrices augue. Mauris dictum facilisis augue. Vivamus luctus egestas leo. Integer malesuada. Donec ipsum massa, ullamcorper in, auctor et, scelerisque sed, est. Nullam lectus justo, vulputate eget mollis sed, tempor sed magna. Nullam faucibus mi quis velit. Fusce suscipit libero eget elit. Fusce nibh. Nunc dapibus tortor vel mi dapibus sollicitudin. Duis condimentum augue id magna semper rutrum. Nunc auctor. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Duis viverra diam non justo. Aenean fermentum risus id tortor. Fusce tellus odio, dapibus id fermentum quis, suscipit id erat. Proin in tellus sit amet nibh dignissim sagittis.</strong></i></p><p><i><strong>Curabitur sagittis hendrerit ante. Itaque earum rerum hic tenetur a sapiente delectus, ut aut reiciendis voluptatibus maiores alias consequatur aut perferendis doloribus asperiores repellat. Duis sapien nunc, commodo et, interdum suscipit, sollicitudin et, dolor. Vestibulum fermentum tortor id mi. Etiam egestas wisi a erat. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Etiam commodo dui eget wisi. Mauris metus. Fusce wisi. Itaque earum rerum hic tenetur a sapiente delectus, ut aut reiciendis voluptatibus maiores alias consequatur aut perferendis doloribus asperiores repellat. Etiam sapien elit, consequat eget, tristique non, venenatis quis, ante. Nullam at arcu a est sollicitudin euismod. Fusce aliquam vestibulum ipsum. Curabitur sagittis hendrerit ante. Proin mattis lacinia justo. Nulla non lectus sed nisl molestie malesuada.</strong></i></p>', '2020-01-08 22:43:56', 1),
(30, 8, 10, 10, 10, 10, 10, 10, '<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Curabitur ligula sapien, pulvinar a vestibulum quis, facilisis vel sapien. Aliquam in lorem sit amet leo accumsan lacinia. Nam sed tellus id magna elementum tincidunt. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Fusce aliquam vestibulum ipsum. Cras pede libero, dapibus nec, pretium sit amet, tempor quis. Nunc dapibus tortor vel mi dapibus sollicitudin. Etiam sapien elit, consequat eget, tristique non, venenatis quis, ante. Integer tempor. Nullam dapibus fermentum ipsum.</p><p><strong>Nullam rhoncus aliquam metus. Pellentesque ipsum. Nulla accumsan, elit sit amet varius semper, nulla mauris mollis quam, tempor suscipit diam nulla vel leo. Etiam sapien elit, consequat eget, tristique non, venenatis quis, ante. Duis bibendum, lectus ut viverra rhoncus, dolor nunc faucibus libero, eget facilisis enim ipsum id lacus. Aliquam id dolor. Integer pellentesque quam vel velit. Nullam eget nisl. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Vivamus porttitor turpis ac leo. Maecenas lorem. Nulla est. Mauris tincidunt sem sed arcu. Aenean id metus id velit ullamcorper pulvinar. Integer tempor.</strong></p><p><strong>Quisque tincidunt scelerisque libero. Etiam quis quam. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Morbi leo mi, nonummy eget tristique non, rhoncus non leo. Nullam lectus justo, vulputate eget mollis sed, tempor sed magna. Nulla est. Vivamus porttitor turpis ac leo. Praesent in mauris eu tortor porttitor accumsan. Sed elit dui, pellentesque a, faucibus vel, interdum nec, diam. Nulla pulvinar eleifend sem. Maecenas ipsum velit, consectetuer eu lobortis ut, dictum at dui.</strong></p>', '2020-01-08 22:43:56', 1),
(31, 8, 11, 8, 9, 8, 8, 5, '<p><strong>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Maecenas lorem. Fusce aliquam vestibulum ipsum. Mauris dictum facilisis augue. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos hymenaeos. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Integer imperdiet lectus quis justo. Morbi scelerisque luctus velit. Nunc auctor. Duis condimentum augue id magna semper rutrum. Maecenas sollicitudin.</strong></p><p><strong>Aliquam erat volutpat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Maecenas fermentum, sem in pharetra pellentesque, velit turpis volutpat ante, in pharetra metus odio a lectus. Vestibulum erat nulla, ullamcorper nec, rutrum non, nonummy ac, erat. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Phasellus enim erat, vestibulum vel, aliquam a, posuere eu, velit. Vestibulum fermentum tortor id mi. Fusce tellus. Vivamus luctus egestas leo. Nunc auctor. Pellentesque ipsum. Fusce nibh. Duis condimentum augue id magna semper rutrum. Nulla quis diam. Quisque porta. Pellentesque ipsum. In enim a arcu imperdiet malesuada. Suspendisse sagittis ultrices augue.</strong></p><p>Aliquam erat volutpat. Aenean fermentum risus id tortor. Donec vitae arcu. Aenean placerat. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Phasellus faucibus molestie nisl. Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. In enim a arcu imperdiet malesuada. Praesent id justo in neque elementum ultrices. Nullam sit amet magna in magna gravida vehicula. Etiam sapien elit, consequat eget, tristique non, venenatis quis, ante. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos hymenaeos. Integer tempor. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Nullam rhoncus aliquam metus. Maecenas fermentum, sem in pharetra pellentesque, velit turpis volutpat ante, in pharetra metus odio a lectus. Nulla quis diam.</p>', '2020-01-08 22:43:56', 1);

-- --------------------------------------------------------

--
-- Struktura tabulky `users`
--

CREATE TABLE `users` (
  `ID_Uzivatel` int(11) NOT NULL,
  `Nickname` varchar(50) NOT NULL,
  `Password` varchar(50) NOT NULL,
  `Jmeno` varchar(50) NOT NULL,
  `Prijmeni` varchar(50) NOT NULL,
  `Email` varchar(100) NOT NULL,
  `Telefon` int(11) NOT NULL,
  `Role` int(11) NOT NULL DEFAULT 3,
  `Active` int(11) NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Vypisuji data pro tabulku `users`
--

INSERT INTO `users` (`ID_Uzivatel`, `Nickname`, `Password`, `Jmeno`, `Prijmeni`, `Email`, `Telefon`, `Role`, `Active`) VALUES
(1, 'Administrator', 'admin', 'Tomáš', 'Linhart', 'tlinhart@students.zcu.cz', 722630875, 1, 1),
(3, 'test01', 'test', 'Testovací', 'Autor 1', 'test@test01.com', 222333444, 3, 1),
(4, 'test02', 'test', 'Testovací', 'Autor 2', 'test@test02.com', 0, 3, 1),
(6, 'test03', 'test', 'Testovací', 'Autor 3', 'user@test03.cz', 13246598, 3, 1),
(7, 'test04', 'test', 'Testovací', 'Autor 4', 'test@test04.com', 0, 3, 1),
(8, 'test05', 'test', 'Testovací', 'Autor 5', 'test@test05.com', 578963457, 3, 1),
(9, 'test11', 'test', 'Testovací', 'Recenzent 1', 'test@test11.com', 0, 2, 1),
(10, 'test12', 'test', 'Testovací', 'Recenzent 2', 'test@test12.com', 0, 2, 1),
(11, 'test13', 'test', 'Testovací', 'Recenzent 3', 'test@test13.com', 0, 2, 1),
(12, 'test14', 'test', 'Testovací', 'Recenzent 4', 'test@test14.com', 0, 2, 1),
(13, 'test15', 'test', 'Testovací', 'Recenzent 5', 'test@test15.com', 0, 2, 1),
(14, 'test21', 'test', 'Testovací', 'Deaktivovaný uživatel 1', 'test@test21.com', 0, 2, 0),
(15, 'test22', 'test', 'Testovací', 'Deaktivovaný uživatel 2', 'test@test22.com', 0, 2, 0);

--
-- Klíče pro exportované tabulky
--

--
-- Klíče pro tabulku `posts`
--
ALTER TABLE `posts`
  ADD PRIMARY KEY (`ID_Post`),
  ADD KEY `PostAutor` (`Autor`);

--
-- Klíče pro tabulku `reviews`
--
ALTER TABLE `reviews`
  ADD PRIMARY KEY (`ID_review`),
  ADD KEY `Reviewer` (`ID_Reviewer`),
  ADD KEY `Post` (`ID_Post`);

--
-- Klíče pro tabulku `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`ID_Uzivatel`);

--
-- AUTO_INCREMENT pro tabulky
--

--
-- AUTO_INCREMENT pro tabulku `posts`
--
ALTER TABLE `posts`
  MODIFY `ID_Post` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT pro tabulku `reviews`
--
ALTER TABLE `reviews`
  MODIFY `ID_review` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=32;

--
-- AUTO_INCREMENT pro tabulku `users`
--
ALTER TABLE `users`
  MODIFY `ID_Uzivatel` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- Omezení pro exportované tabulky
--

--
-- Omezení pro tabulku `posts`
--
ALTER TABLE `posts`
  ADD CONSTRAINT `PostAutor` FOREIGN KEY (`Autor`) REFERENCES `users` (`ID_Uzivatel`);

--
-- Omezení pro tabulku `reviews`
--
ALTER TABLE `reviews`
  ADD CONSTRAINT `Post` FOREIGN KEY (`ID_Post`) REFERENCES `posts` (`ID_Post`),
  ADD CONSTRAINT `Reviewer` FOREIGN KEY (`ID_Reviewer`) REFERENCES `users` (`ID_Uzivatel`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
